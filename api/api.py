import asyncpg
import asyncio
import base64
import vertexai
from vertexai.generative_models import GenerativeModel, Part, FinishReason
import vertexai.preview.generative_models as generative_models
from fastapi import FastAPI
from langchain_google_vertexai import VertexAIEmbeddings
from pgvector.asyncpg import register_vector
from pydantic import BaseModel


class SemanticRequest(BaseModel):
    question: str
    limit: int = 3 
    threshold: float = 0.5

EMBEDDING_MODEL_NAME = "text-multilingual-embedding-002"
project_id = "{Project ID}"
region = "{Region}"  # @param {type:"string"}
database_host = "{Host IP Address}" # @param {type:"string"}
database_name = "{Database Name}"  # @param {type:"string"}
database_user = "{User Name}"
database_password = "{Password}"


promptTemplate = """Use all the information from the context to answer new question. Describe 解決方案 as much as possible. If you don't see it in the context , just say you \
didn't find the answer in the given data. Don't make things up. Using Mandarin to answer.

Context:
```{context}
```
Question:
```{question}```

Answer:"""

contextTemplate = """

"""

mapping = {
    'system' : '系統',
    'problem' : '問題',
    'solution' : '解決方案',
}

def generateContextFromRows(rows):
    tmp_list = [', '.join([f"{mapping[key]}: {value}" for key, value in dictionary.items()]) for dictionary in rows]
    return '; '.join(tmp_list)


def llmagent(prompt):
    vertexai.init(project=project_id, location=region)
    model = GenerativeModel(
       "gemini-1.5-pro-001",
    )
    generation_config = {
        "max_output_tokens": 8192,
        "top_p": 0.95,
    }

    safety_settings = {
        generative_models.HarmCategory.HARM_CATEGORY_HATE_SPEECH: generative_models.HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
        generative_models.HarmCategory.HARM_CATEGORY_DANGEROUS_CONTENT: generative_models.HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
        generative_models.HarmCategory.HARM_CATEGORY_SEXUALLY_EXPLICIT: generative_models.HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
        generative_models.HarmCategory.HARM_CATEGORY_HARASSMENT: generative_models.HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
    }

    responses = model.generate_content(
        [prompt],
        generation_config=generation_config,
        safety_settings=safety_settings,
        stream=True,
    )
    answer = ''
    for response in responses:
        answer = answer + response.text
    return answer

app = FastAPI()


async def fetch_users(conn: asyncpg.Connection, query_embedding, similarity_threshold: float, limit: int):
    query = """
            SELECT system, problem, solution
            FROM problem_and_solution_vector_table
            WHERE (embedding <=> $1) < $2
            ORDER BY (embedding <=> $1) 
            LIMIT $3
            """
    rows = await conn.fetch(query, query_embedding, similarity_threshold, limit)
    result = [dict(r) for r in rows]
    return result

@app.on_event("startup")
async def startup():
    app.db = await asyncpg.create_pool(user=database_user, password=database_password,
                                 database=database_name, host=database_host)

@app.on_event("shutdown")
async def shutdown():
    await app.db.close()

@app.get("/semantic")
async def get_users(threshold: float, limit: int):
    question = '無法正常使用' 
    embed_service = VertexAIEmbeddings(model_name=EMBEDDING_MODEL_NAME)
    query_embedding = embed_service.embed_query(question)
    async with app.db.acquire() as conn:
        await register_vector(conn)
        rows = await fetch_users(conn, query_embedding, threshold, limit)
        prompt = promptTemplate.format(context=generateContextFromRows(rows), question= question)
        return {'message':llmagent(prompt)}

@app.post("/semantic")
async def get_users(reqBody: SemanticRequest):
    embed_service = VertexAIEmbeddings(model_name=EMBEDDING_MODEL_NAME)
    query_embedding = embed_service.embed_query(reqBody.question)
    async with app.db.acquire() as conn:
        await register_vector(conn)
        rows = await fetch_users(conn, query_embedding, reqBody.threshold, reqBody.limit)
        prompt = promptTemplate.format(context=generateContextFromRows(rows), question=reqBody.question)
        print(prompt)
        return {'message':llmagent(prompt)}
