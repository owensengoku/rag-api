# Data Process


## Steps

### Step 1
Using csv_handler.py to convert demo-data.csv to demo-data-embeded.csv
( append embedding column, the value is the embedding of original row )

### Step 2
Using insertdb.py to insert demo-data-embeded.csv into AlloyDB ( postgresql)
( It assume table existed )