database_host = "{HOST IP ADDRESS}" # @param {type:"string"}
database_name = "{database name}"  # @param {type:"string"}
database_user = "{UserName}"
database_password = "{Password}"

x
from google.cloud.alloydb.connector import Connector, IPTypes
import sqlalchemy


pool = sqlalchemy.create_engine(
        # Equivalent URL:
        # postgresql+pg8000://<db_user>:<db_pass>@<db_host>:<db_port>/<db_name>
        sqlalchemy.engine.url.URL.create(
            drivername="postgresql+pg8000",
            username=database_user,
            password=database_password,
            host=database_host,
            port="5432",
            database=database_name,
        ),
        isolation_level="AUTOCOMMIT"
    )


import pandas as pd

embedded_data = "demo-data-embedded.csv"
df = pd.read_csv(embedded_data)
insert_data_cmd = sqlalchemy.text(
    """
    INSERT INTO problem_and_solution_vector_table VALUES (:id, :system, :problem, :solution,
      :embedding)
    """
)

parameter_map = [
    {
        "id": row["id"],
        "system": row["system"],
        "problem": row["problem"],
        "solution": row["solution"],
        "embedding": row["embedding"],
    }
    for index, row in df.iterrows()
]

with pool.connect() as db_conn:
    db_conn.execute(
        insert_data_cmd,
        parameter_map,
    )
    db_conn.commit()

