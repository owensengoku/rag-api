import csv

from langchain_google_vertexai import VertexAIEmbeddings
from typing import Optional
from pydantic import BaseModel

EMBEDDING_MODEL_NAME="text-multilingual-embedding-002"

class ProblemSolution(BaseModel):
    id: int
    system: str
    solution: str
    problem: str
    embedding: Optional[list[float]] = None


embed_service = VertexAIEmbeddings(model_name=EMBEDDING_MODEL_NAME)

psenities: list[ProblemSolution] = []

rawMessageFormat="系統：{}, 問題：{}, 解決方案：{}"

with open("demo-data.csv", "r") as f:
    reader = csv.DictReader(f, delimiter=",")
    for line in reader:
        psenity = ProblemSolution.model_validate(line)
        psenity.embedding = embed_service.embed_query(rawMessageFormat.format(psenity.system, psenity.problem, psenity.solution))
        psenities.append(psenity)


with open("demo-data-embedded.csv", "w") as f:
    col_names = [
        "id",
        "system",
        "solution",
        "problem",
        "embedding",
    ]
    writer = csv.DictWriter(f, col_names, delimiter=",")
    writer.writeheader()
    for psenity in psenities:
        writer.writerow(psenity.model_dump())

print("Wrote data to CSV.")

